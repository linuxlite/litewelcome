function WelcomeCtrl($scope) {

}

function checkboxCtr() {
  if (window.$) {
    // if has jQuery
    $('#toggle_autostart').click(function() {
      var checkbox = $('#autostart')
      checkbox.toggleClass("icon-check")
      checkbox.toggleClass("icon-check-empty")
    })
  } else {
    document.querySelector('#toggle_autostart').onclick = function() {
      var checkbox = document.querySelector('#autostart')
      if (checkbox.classList.value.indexOf('icon-check-empty') >= 0) {
        checkbox.classList.value = "icon icon-check"
      } else {
        checkbox.classList.value = "icon icon-check-empty"
      }
    }
  }
}

checkboxCtr()
