Lite Welcome
=================

The Linux Lite Welcome Screen.

![](https://imgur.com/jIZjEVh.png)

## License ![License](https://img.shields.io/badge/license-GPLv2-green.svg)

This project is under the GPLv2 license. Unless otherwise stated in individual files.

## Authors
- [Jerry Bezencon](https://github.com/linuxlite/)
- [Johnathan "ShaggyTwoDope" Jenkins](https://github.com/shaggytwodope/)
- [Misko-2083](https://github.com/Misko-2083/)
